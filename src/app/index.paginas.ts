import { from } from 'rxjs';
import { BodyComponent } from './components/body/body.component';
import { Unidad1Component } from './components/unidad1/unidad1.component';
import { Unidad2Component } from './components/unidad2/unidad2.component';
import { Unidad3Component } from './components/unidad3/unidad3.component';


export { BodyComponent} from './components/body/body.component';
export { Unidad1Component } from './components/unidad1/unidad1.component';
export {Unidad2Component} from './components/unidad2/unidad2.component';
export {Unidad3Component} from'./components/unidad3/unidad3.component';