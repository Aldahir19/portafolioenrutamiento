import { Routes, RouterModule } from '@angular/router';
import {Unidad1Component,Unidad2Component,Unidad3Component, BodyComponent} from "./index.paginas"
const app_routes: Routes = [
{path:'home', component: BodyComponent},
{path:'unidad1', component: Unidad1Component},
{path:'unidad2', component: Unidad2Component},
{path:'unidad3',component: Unidad3Component}

];

export const app_routing = RouterModule.forRoot(app_routes);